require 'cgi'
require 'active_record'
require 'action_dispatch'
require 'action_controller'

ActiveRecord::Base.establish_connection adapter: 'sqlite3', database: 'notebook.sqlite3'

class Note < ActiveRecord::Base
end

class NotesController < ActionController::Base
  def index
    @notes = Note.all
  end

  def create
    Note.create content: params[:content]

    redirect_to '/show-notes', status: :see_other
  end
end

router = ActionDispatch::Routing::RouteSet.new
router.draw do
  get '/show-notes',   to: NotesController.action(:index)
  post '/create-note', to: NotesController.action(:create)
end

Notebook = router

